import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss'],
})
export class CatalogComponent implements OnInit {
  list = [
    {
      text: 'Департамент добропорядочности',
    },
    {
      text: 'Департамент партнерства',
    },
    {
      text: 'Департамент превенции',
    },
    {
      text: 'Департамент (по расследованию дел в государственном секторе)',
    },
    {
      text: 'Департамент (по расследованию дел в квазигосударственном секторе)',
    },
    {
      text: 'Департамент(по оперативной деятельности в государственном секторе)',
    },
    {
      text: 'Департамент (по оперативной деятельности в квазигосударственном секторе)',
    },
    {
      text: 'Департамент (по специальной технической поддержке оперативно-следственной деятельности)',
    },
    {
      text: 'Департамент стратегии и оперативного управления (Штаб)',
    },
  ];
  constructor(private router: Router) {}

  ngOnInit(): void {}

  goBack() {
    this.router.navigate(['/']);
  }
}
