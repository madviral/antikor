export interface Employee {
  id: number;
  lastName: string;
  firstName: string;
  middleName: string;
  iin: string;
  department: string;
  division: string;
  position: string;
  positionCategory: string;
  photo: string;
}
