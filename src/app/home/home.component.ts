import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../models';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  navList = [
    {
      icon: '/assets/icons/item-add.svg',
      text: 'Новый сотрудник',
      link: '/',
    },
    {
      icon: '/assets/icons/analytics.svg',
      text: 'Аналитика',
      link: '/',
    },
    {
      icon: '/assets/icons/book.svg',
      text: 'Справочник',
      link: '/catalog',
    },
    {
      icon: '/assets/icons/item-add.svg',
      text: 'Анкета',
      link: '/',
    },
    {
      icon: '/assets/icons/item-add.svg',
      text: 'Опросы',
      link: '/',
    },
  ];
  empList$: Observable<Employee[]>;

  constructor(private employeeService: EmployeeService) {}

  ngOnInit() {
    this.empList$ = this.employeeService.getEmployees();
  }
}
