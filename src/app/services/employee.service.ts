import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { CommonModel, Employee } from '../models';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  private apiUrl = 'http://anticor.idet.kz/hrwebapi/';
  constructor(private http: HttpClient) {}

  getEmployees(): Observable<Employee[]> {
    return this.http.get(`${this.apiUrl}employee/getemployees`).pipe(map((data: CommonModel) => data.items));
  }
}
